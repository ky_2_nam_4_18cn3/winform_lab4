﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            if (!ValidatorForm())
            {
                MessageBox.Show("Chưa nhập tên khách hàng!");
                return;
            }

            double total = 0;

            switch (chkClean.CheckState)
            {
                case CheckState.Checked:
                    total += 100000;
                    break;
            }

            switch (chkWhitening.CheckState)
            {
                case CheckState.Checked:
                    total += 1200000;
                    break;
            }

            switch (chkXRay.CheckState)
            {
                case CheckState.Checked:
                    total += 200000;
                    break;
            }

            if (numFilling.Value != 0)
            {
                var numFillingTmp = 80000 * numFilling.Value;
                total += (int)numFillingTmp;
            }

            txtTotal.Text = $"${total.ToString()}";
        }

        private bool ValidatorForm()
        {
            if (String.IsNullOrEmpty(txtName.Text))
            {
                return false;
            }

            return true;
        }
    }
}
